#include <iostream>

// Function for output even numbers '2k' or odd numbers '2k+1'
void Print_EvenNumbers(bool Even, int Number)
{
    for (int i = !Even; i <= Number; i += 2 )   // !Even = 0, Even = 1
    {
        std::cout << i << " ";

        //if ( (i % 2 == 0) && (Even) )
        //{
        //    std::cout << i << " ";            // Output even numbers if Even == true
        //}
        //else if ((i % 2 != 0) && !(Even))
        //{
        //    std::cout << i << " ";            // Output even numbers if Even == false
        //}
    }
    std::cout << "\n \n";
}



int main()
{
    int Number = 0;

    std::cout << "Enter number N = ";
    std::cin >> Number;
    std::cout << "\n";

    // Output odd numbers '2k + 1'
    std::cout << "Odd numbers: \n";
    for (int i = 0; i <= Number; i++)
    {
        if (i % 2 != 0)
        {
            std::cout << i << " ";
        }
    }
    std::cout << "\n \n";

    Print_EvenNumbers(true, Number);            // Call function for output even numbers

    Print_EvenNumbers(false, Number);           // Call function for output odd numbers


}

